---
title:  "Resizing volumes on Ubuntu Server"
date:   2022-02-01 12:00:00 +0000
tags: linux
author: Trevor Vallender
layout: post
---

I recently created a new Ubuntu Server VM and failed to make the virtual hard
disk big enough. Despite the solution being simple, I spent some time this
morning wrestling with finding the correct method to fix this, so am documenting
it here for future use.

Firstly, resize the virtual hard disk. In VirtualBox, which I am using, this is
done in Tools > Hard disks > Size. From there, you need to resize the physical
volume.

{% highlight shell %}
$ pvs # This will give you the PV name required, e.g. /dev/sda2
$ pvresize <PV Name>
$ df -h # Find the root logical volume name (listed as / filesystem)
$ lvextend -r -l +100%FREE <LV name>
{% endhighlight %}